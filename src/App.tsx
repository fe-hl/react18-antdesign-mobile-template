import React, { Suspense } from 'react';
import { RouterProvider } from 'react-router-dom';
import router from './router';
import { DotLoading } from 'antd-mobile';

const FallbackLoading: React.FC = () => {
  return (
    <div
      style={{
        position: 'fixed',
        left: 0,
        top: 0,
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <DotLoading color='primary' style={{ fontSize: 24 }} />
    </div>
  );
};

function App() {
  return (
    <Suspense fallback={<FallbackLoading />}>
      <RouterProvider router={router}></RouterProvider>
    </Suspense>
  );
}

export default App;
