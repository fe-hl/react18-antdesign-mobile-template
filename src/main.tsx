import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import App from './App.tsx';
import '@/assets/css/index.scss';
import 'virtual:svg-icons-register';
import { loadScript, queryUrlParams } from '@fe-hl/shared';
import store from './store';
import Locales from './locales/index.tsx';

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <Provider store={store}>
    {/* 本地开发环境取消并发模式 */}
    {import.meta.env.MODE === 'development' ? (
      <Locales>
        <App />
      </Locales>
    ) : (
      <React.StrictMode>
        <Locales>
          <App />
        </Locales>
      </React.StrictMode>
    )}
  </Provider>,
);

if (queryUrlParams().debugger == 'debugger') {
  loadScript(
    `${location.origin + import.meta.env.VITE_API_BASE_URL}/vconsole.min.js`,
    () => {
      new window['VConsole']();
    },
  );
}
