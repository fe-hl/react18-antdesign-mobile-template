import { UserInfo } from '../types';
import { httpGet, httpPost } from '../utils/Httprequest';

export const loginApi = (data: {
  phone: string;
  code: string;
}): Promise<UserInfo> => {
  return httpPost('/user/login', data);
};

export const userListApi = () => {
  return httpGet(`/user/list`);
};

export const userListbApi = () => {
  return httpGet(`/user/listb`);
};

// 刷新Token
export const refreshToken = (): Promise<{
  token: string;
  refreshToken: string;
}> => {
  return httpGet(
    '/user/refresh',
    {},
    {
      isLoading: false,
      isToast: false,
      isCancelReq: false,
      isDeduplication: false,
    },
  );
};
