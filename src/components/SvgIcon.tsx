import { memo } from 'react';

interface Props {
  size?: number | [number, number];
  color?: string;
  name: string;
}

function SvgIcon(props: Props) {
  const symbolId = `#icon-${props.name}`;
  const iconStyle = () => {
    const styles: { color?: string; width?: string; height?: string } = {};
    if (props.color) {
      styles.color = props.color;
    }
    if (props.size) {
      if (!Array.isArray(props.size)) {
        styles.width = `${props.size}px`;
        styles.height = `${props.size}px`;
      }
      const sizes = props.size as Array<number>;
      if (sizes[0]) {
        styles.width = `${sizes[0]}px`;
        styles.height = `${sizes[0]}px`;
      }
      if (sizes[1]) {
        styles.height = `${sizes[1]}px`;
      }
    }
    return styles;
  };
  return (
    <svg style={iconStyle()}>
      <use href={symbolId} />
    </svg>
  );
}

export default memo(SvgIcon);
