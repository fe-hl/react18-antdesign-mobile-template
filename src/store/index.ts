import { configureStore, combineReducers } from '@reduxjs/toolkit';
import logger from 'redux-logger';
import userReducer from './user';
import languageReducer from './language';
import cancelReqReducer from './cancelReq';
import themeReducer from './theme';
import reduxPersistence from '../middleware/redux-persistence';
import { STORAGE, storageGet } from '@fe-hl/shared';
import { IState } from '../types';

const rootReducer = combineReducers({
  user: userReducer,
  language: languageReducer,
  cancelReq: cancelReqReducer,
  theme: themeReducer,
});
const store = configureStore({
  devTools: import.meta.env.MODE === 'development',
  preloadedState: storageGet('REDUX_PERSISTENCE', STORAGE.SESSION) as IState,
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(reduxPersistence).concat(logger),
});
export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;

export default store;
