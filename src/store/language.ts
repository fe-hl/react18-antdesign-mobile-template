import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Language } from '../types';

const initialState: Language = {
  type: 'en-US',
};

const languageModule = createSlice({
  name: 'language',
  initialState,
  reducers: {
    updateLanguage: (state, action: PayloadAction<Language>) => {
      state.type = action.payload.type;
    },
  },
});

export const { updateLanguage } = languageModule.actions;
export default languageModule.reducer;
