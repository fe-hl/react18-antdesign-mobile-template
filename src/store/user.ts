import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { UserInfo } from '../types';

const initialState: UserInfo = {
  username: '',
  token: '',
  refreshToken: '',
  userId: '',
};

const userModule = createSlice({
  name: 'user',
  initialState,
  reducers: {
    updateUserInfo: (state, action: PayloadAction<Partial<UserInfo>>) => {
      Object.assign(state, action.payload);
    },
    logout: (state, action: PayloadAction<string>) => {
      Object.assign(state, action.payload);
    },
  },
});

export const { updateUserInfo, logout } = userModule.actions;
export default userModule.reducer;
