import { queryUrlParams } from '@fe-hl/shared';
import { NavigateFunction, useNavigate, useParams } from 'react-router-dom';
interface Routes {
  router: NavigateFunction;
  params: {
    [propname: string]: unknown;
  };
}
export const useRoutes = (): Routes => {
  const router = useNavigate();
  return {
    params: {
      ...useParams(),
      ...queryUrlParams(),
    },
    router,
  };
};
