import { Canceler } from 'axios';

// store
export interface UserInfo {
  username: string;
  token: string;
  refreshToken: string;
  userId: string;
}

export type Typelanguage = 'en-US' | 'zh-CN';

export interface Language {
  type: Typelanguage;
}

export interface CancelReq {
  queue: Canceler[];
}

export type TypeTheme = 'light' | 'dark' | 'system';

export interface Theme {
  mode: string;
}

export interface IState {
  user: UserInfo;
  language: Language;
  cancelReq: CancelReq;
  theme: Theme;
}
