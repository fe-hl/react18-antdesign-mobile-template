import { IntlProvider } from 'react-intl';

import { ReactNode } from 'react';
import { ConfigProvider } from 'antd-mobile';
import zhCN from './zh-CN.json';
import enUS from './en-US.json';
// antd
import zhCNAntd from 'antd-mobile/es/locales/zh-CN';
import enUSAntd from 'antd-mobile/es/locales/en-US';
import { useSelector } from 'react-redux';
import { RootState } from '../store';

export const messages = {
  'zh-CN': zhCN,
  'en-US': enUS,
};

const mappingLocale = {
  'zh-CN': zhCNAntd,
  'en-US': enUSAntd,
};

interface Props {
  children: ReactNode;
}

const Locales: React.FC<Props> = (props) => {
  const language = useSelector((state: RootState) => state.language.type);
  return (
    <IntlProvider messages={messages[language]} locale={language}>
      <ConfigProvider locale={mappingLocale[language]}>
        {props.children}
      </ConfigProvider>
    </IntlProvider>
  );
};

export default Locales;
