import { memo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { Button, Divider, Space, Tag } from 'antd-mobile';
import SvgIcon from '../../components/SvgIcon';

import { updateLanguage } from '../../store/language';
import { updateUserInfo } from '../../store/user';
import { updateTheme } from '../../store/theme';
import { loginApi, userListApi, userListbApi } from '../../apis';
import { useRoutes } from '../../hooks';
import { RootState } from '../../store';
import styles from './index.module.scss';
const Demo: React.FC = () => {
  const theme = useSelector((state: RootState) => state.theme.mode);
  const dispatch = useDispatch();
  const { router, params } = useRoutes();
  const login = async () => {
    const userInfo = await loginApi({ phone: '17621222222', code: '12355' });
    dispatch(updateUserInfo(userInfo));
    router(params.redirect ? params.redirect : '/home');
  };

  const concurrency = () => {
    userListApi();
    userListbApi();
  };

  return (
    <>
      <Space direction='vertical' style={{ padding: '10px', width: '100vw' }}>
        <Tag color='success' fill='outline'>
          多语言
        </Tag>
        <Space>
          <Button
            onClick={() => dispatch(updateLanguage({ type: 'zh-CN' }))}
            color='primary'
          >
            中文
          </Button>
          <Button
            onClick={() => dispatch(updateLanguage({ type: 'en-US' }))}
            color='primary'
          >
            英文
          </Button>
          <Button color='primary' onClick={login}>
            <FormattedMessage id='login.text' />
          </Button>
        </Space>
        <Divider />
        <Tag color='success' fill='outline'>
          主题
        </Tag>
        <Space>
          <Button
            color='primary'
            onClick={() => dispatch(updateTheme('light'))}
          >
            雅白
          </Button>
          <Button color='primary' onClick={() => dispatch(updateTheme('dark'))}>
            暗黑
          </Button>
          <Button
            color='primary'
            onClick={() => dispatch(updateTheme('system'))}
          >
            跟随系统
          </Button>
          <div className={styles.theme}>theme-{theme}</div>
        </Space>
        <Divider />
        <Tag color='success' fill='outline'>
          svgIcon
        </Tag>
        <Space>
          <SvgIcon name='vue' size={50}></SvgIcon>
          <SvgIcon name='social-wechat' color='red' size={[50, 50]}></SvgIcon>
        </Space>
        <Divider />
        <Tag color='success' fill='outline'>
          并发请求登录过期-无感知刷新
        </Tag>
        <Space>
          <Button color='primary' onClick={() => concurrency()}>
            并发请求
          </Button>
        </Space>
      </Space>
    </>
  );
};

export default memo(Demo);
