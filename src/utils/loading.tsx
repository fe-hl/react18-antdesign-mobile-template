import { DotLoading } from 'antd-mobile';
import ReactDOM from 'react-dom/client';
export const showLoading = () => {
  const loading = document.createElement('div');
  loading.setAttribute('id', 'loading');
  ReactDOM.createRoot(loading).render(
    <DotLoading color='primary' style={{ fontSize: 30 }} />,
  );
  document.body.appendChild(loading);
};

export const closeLoading = () => {
  document.body.removeChild(
    document.getElementById('loading') as HTMLDivElement,
  );
};
